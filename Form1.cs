﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassG;

using System.Data.SqlClient;

using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace testZ
{
    public partial class Form1 : Form
    {
		//соеди к БД постоянное. БД  MS SQL 2008
        public const string toDB = "Data Source=(local); Integrated Security=SSPI; Pooling=False";

        public Form1()
        {
            InitializeComponent();
            this.Text = Gastroli.url + " Екатеринбург"; //name url = sity 
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            System.Diagnostics.Process.Start(Gastroli.url); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                
                toolStripProgressBar1.Visible = true;
                toolStripProgressBar1.Minimum = 0;
                toolStripProgressBar1.Maximum = 100;
                toolStripProgressBar1.Value = 1;
                toolStripProgressBar1.Step = 1;

                Gastroli test = new Gastroli();
                string inf=(test.info());
                
                string[] t=test.rec(inf, Gastroli.patternD);
                               
                for (int i = 0; i <= t.Count(); i++)
                {
                    string minfo;
                    if (i == 0)
                     {
                        minfo = inf.Substring(0, (inf.IndexOf(t[i])) );
                        toolStripProgressBar1.PerformStep();
                      }
                   else if (i == t.Count())
                        {
                            minfo = inf.Substring(inf.IndexOf(t[i-1]));
                            toolStripProgressBar1.PerformStep();
                         }
                     else   minfo = inf.Substring(inf.IndexOf(t[i-1]), (inf.IndexOf(t[i])-inf.IndexOf(t[i-1])) );
                    
                    string[] mS = test.rec(minfo, Gastroli.patternS);
                    string[] mO = test.rec(minfo, Gastroli.patternO);

                    toolStripProgressBar1.Step = 13;
                    toolStripProgressBar1.PerformStep();

                    string[] day = test.rec(minfo, Gastroli.patternB);
                    
                    for (int j = 0; j < mS.Count();j++)
                    {
                        if (i == 0)
                        {
                            if (mO.Count() == j) dataGridView1.Rows.Add(t[i], mO[j - 1].Trim(), mS[j]);
                            else
                                dataGridView1.Rows.Add(day[i], mO[j].Trim(), mS[j]);
                            toolStripProgressBar1.PerformStep();
                        }
                        else if (mO.Count() == j) dataGridView1.Rows.Add(t[i - 1], mO[j - 1].Trim(), mS[j]);
                        else dataGridView1.Rows.Add(t[i - 1], mO[j].Trim(), mS[j]);
                    }
                    
                }
               
            }

            catch (Exception ex)
            {
				//вывод ошибки и сброс прогрессБара
                MessageBox.Show( ex.Message,"Error");
                toolStripProgressBar1.Value = 0;
            }
        
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            toolStripStatusLabel1.Text = dataGridView1.CurrentCell.Value.ToString();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.GetCellCount(DataGridViewElementStates.Selected) > 0)
                Clipboard.SetDataObject(this.dataGridView1.GetClipboardContent());
            else
                this.toolStripStatusLabel1.Text = "НЕ ВЫБРАНЫ ЯЧЕЙКИ!";
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Maximum = 8;
            toolStripProgressBar1.Value = 1;
            toolStripProgressBar1.Step = 1;

			//cоед. к БД локально.
            using (SqlConnection connection=new SqlConnection
                (@"Data Source=(local); Integrated Security=SSPI; Pooling=False") )
            {
                try
                {
                    toolStripProgressBar1.PerformStep();
                    connection.Open();
                        toolStripProgressBar1.PerformStep();
                    connection.Close();
                        toolStripProgressBar1.PerformStep();

                        if (dataGridView1.RowCount > 0)
                        {
                            button4.Enabled = true;
                            toolStripProgressBar1.PerformStep();

                            find_ToolStripMenuItem.Enabled = true;
                            toolStripProgressBar1.PerformStep();
                                                       
                        }
                        toolStripProgressBar1.Value = toolStripProgressBar1.Maximum;
                   MessageBox.Show("Есть подключение к БД", "connection to BD"); 

                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("Нет подключения к серверу: " + ex.Message);
                    toolStripProgressBar1.Value = 0;
                }
                
            }

        }

        private void button4_Click(object sender, EventArgs e)
        { 
            try
            {
                SqlConnection sqlADB = new SqlConnection(@toDB);
                
                
				//заполнение локал БД 
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    sqlADB.Open();
                    using (SqlCommand ravno = new SqlCommand("SELECT * FROM tur.dbo.ekb WHERE date=@pd and mesto=@pm and event=@pE"))
                    {
                        ravno.Parameters.AddWithValue("@pd", dataGridView1[0, i].Value.ToString());
                        ravno.Parameters.AddWithValue("@pm", dataGridView1[1, i].Value.ToString());
                        ravno.Parameters.AddWithValue("@pE", dataGridView1[2, i].Value.ToString());
                        ravno.Connection = sqlADB;
                        
                        using (SqlDataReader dr = ravno.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            
                            if (dr.HasRows == false)
                            {
                                sqlADB.Close();
                                SqlCommand com = new SqlCommand("INSERT INTO tur.dbo.ekb (date,mesto,event) VALUES(@d, @m, @e)",
                     sqlADB);
                                com.Parameters.AddWithValue("@d", dataGridView1[0, i].Value.ToString());
                                com.Parameters.AddWithValue("@m", dataGridView1[1, i].Value.ToString());
                                com.Parameters.AddWithValue("@e", dataGridView1[2, i].Value.ToString());
                                sqlADB.Open();
                                com.ExecuteNonQuery();
                            }
                            
                            dr.Close();
                        }
                    } sqlADB.Close();
                  }
                
                 
                MessageBox.Show("Данные обновлены", "Answer_Server");
              }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

                        
        }

        private void find_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string SelSql ="SELECT * FROM tur.dbo.ekb WHERE ";
                
                for (int i = 0; i < dataGridView1.SelectedCells.Count;i++)
                {
                    if (dataGridView1.SelectedCells[i].ColumnIndex==0)
                    SelSql+="date='"+dataGridView1.SelectedCells[i].Value.ToString()+"' ";

                    if (dataGridView1.SelectedCells[i].ColumnIndex == 1)
                        SelSql += "mesto='" + dataGridView1.SelectedCells[i].Value.ToString() + "' ";

                    if (dataGridView1.SelectedCells[i].ColumnIndex == 2)
                        SelSql += "event='" + dataGridView1.SelectedCells[i].Value.ToString() + "' ";

                    if (i<((dataGridView1.SelectedCells.Count)-1)) SelSql+=" and "; 


                }

                using (SqlConnection vcon = new SqlConnection(@toDB))
                {
                    vcon.Open();
                   
                    dataGridView1.Rows.Clear();
                    SqlCommand cmd=new SqlCommand(SelSql,vcon);
                    
                    using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {   
                        //int i=0;
                        while (dr.Read())
                        {
                            dataGridView1.Rows.Add(dr.GetValue(0).ToString(), dr.GetValue(1).ToString(), dr.GetValue(2).ToString());
                        }
                    }
                }
               

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

    }
}